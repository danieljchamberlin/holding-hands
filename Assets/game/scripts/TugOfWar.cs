﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TugOfWar : MonoBehaviour {

    public GameObject needle;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
    }
 
    public void TugTheRope(bool isRight, float tugForceValue)
    {
        float tugForce = tugForceValue;

        if (isRight)
        {
            tugForce = Mathf.Abs(tugForce);
        } else { tugForce = -(Mathf.Abs(tugForce)); }

        rb.AddTorque(tugForce);
    }

    public void KillTheWotion()
    {
        rb.angularVelocity = 0;
    }
}
