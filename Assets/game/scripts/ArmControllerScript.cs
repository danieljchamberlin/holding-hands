/*
 * Copyright 2014, Gregg Tavares.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Gregg Tavares. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HappyFunTimes;
using HutongGames.PlayMaker;

public class ArmControllerScript : MonoBehaviour {

    public float tugForce = 5f;
    public bool isTeamRight = true;
    public AudioClip[] gruntSoundClips;
    public AudioClip[] winSounds;
    public AudioClip[] loseSounds;

    private PlayMakerFSM gamemanagerfsm;
    private GameObject needleGO;
    private AudioSource needleAudio;
    private ParticleSystem sweatPS;
    private Renderer leftArmRenderer;
    private Renderer rightArmRenderer;
    private Color rightColor;
    private Color leftColor;

    private int m_id;
    private HFTGamepad m_gamepad;
    private HFTInput m_hftInput;
    private HFTSoundPlayer m_soundPlayer;
    private static Dictionary<int, bool> s_ids= new Dictionary<int, bool>();
    private bool canPlay = false;

    private static int m_rightTeamPlayerCount = 0;
    private static int m_leftTeamPlayerCount = 0;
    private static int m_playerNumber = 0;

    void Start ()
    {
        // Find an empty id;
        bool foo = false;
        for (int ii = 0; ii < 1000; ++ii) {
            if (!s_ids.TryGetValue(ii, out foo)) {
                m_id = ii;
                s_ids[ii] = true;
                break;
            }
        }

        m_gamepad = GetComponent<HFTGamepad>();
        m_hftInput = GetComponent<HFTInput>();
        m_soundPlayer = GetComponent<HFTSoundPlayer>();

        // get arm colors from arms
        rightArmRenderer = GameObject.FindGameObjectWithTag("RightArm").GetComponent<Renderer>();
        leftArmRenderer = GameObject.FindGameObjectWithTag("LeftArm").GetComponent<Renderer>();
        rightColor = rightArmRenderer.material.color;
        leftColor = leftArmRenderer.material.color;

        // choose right or left team based on current # of players on teams, try to balance
        m_playerNumber++;

        // test even or odd
        if (m_playerNumber % 2 == 0) { m_leftTeamPlayerCount++; }
        else if (m_playerNumber % 2 != 0) { m_rightTeamPlayerCount++; }

        if (m_leftTeamPlayerCount >= m_rightTeamPlayerCount) {
            isTeamRight = true;
            SetColor(rightColor);
        } else if (m_rightTeamPlayerCount > m_leftTeamPlayerCount) {
            isTeamRight = false;
            SetColor(leftColor);
        }

        // find game objects
        gamemanagerfsm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayMakerFSM>();
        needleGO = GameObject.FindGameObjectWithTag("needle");
        needleAudio = needleGO.GetComponent<AudioSource>();
        sweatPS = GameObject.Find("psSweat").GetComponent<ParticleSystem>();

        // Delete ourselves if disconnected
        m_gamepad.OnDisconnect += Remove;
    }

    void Remove()
    {
        s_ids.Remove(m_id);
        Destroy(gameObject);
    }

    void Update()
    {
        // Right Team input
        if (!isTeamRight)
        {
            if (Input.GetButtonDown("Fire1") || m_hftInput.GetButtonDown("Fire1"))
            {
                // test if we can play
                canPlay = gamemanagerfsm.FsmVariables.GetFsmBool("canPlay").Value;
                if (!canPlay) { return; }

                // sweat
                if (!sweatPS.isPlaying) { sweatPS.Play(); }

                needleGO.GetComponent<TugOfWar>().TugTheRope(true, tugForce); // rotate towards right
                // play random grunt sfx
                PlayGrunt();
            }
        }

        // Left Team input
        if (isTeamRight)
        {
            if (Input.GetButtonDown("Fire1") || m_hftInput.GetButtonDown("Fire1"))
            {
                // test if we can play
                canPlay = gamemanagerfsm.FsmVariables.GetFsmBool("canPlay").Value;
                if (!canPlay) { return; }

                // sweat
                if (!sweatPS.isPlaying) { sweatPS.Play(); }

                needleGO.GetComponent<TugOfWar>().TugTheRope(false, tugForce); // rotate towards left
                // play random grunt sfx
                PlayGrunt();
            }
        }
    }

    void PlayGrunt()
    {
        // play random grunt sfx
        int randomGrunt = Random.Range(0, gruntSoundClips.Length);
        m_soundPlayer.PlaySound(gruntSoundClips[randomGrunt].name); // on phone
        Debug.Log( gruntSoundClips[randomGrunt].name );

        if (needleAudio.isPlaying) return;
        needleAudio.PlayOneShot(gruntSoundClips[randomGrunt]); // in unity
    }

    public void PlayWin()
    {
        // play random grunt sfx
        int randomWinSound = Random.Range(0, winSounds.Length);
        m_soundPlayer.PlaySound(winSounds[randomWinSound].name); // on phone
        Debug.Log(winSounds[randomWinSound].name);
    }

    public void PlayLose()
    {
        // play random grunt sfx
        int randomLoseSound = Random.Range(0, loseSounds.Length);
        m_soundPlayer.PlaySound(loseSounds[randomLoseSound].name); // on phone
        Debug.Log(loseSounds[randomLoseSound].name);
    }

    void SetColor(Color playerColor)
    {
        // Tell the gamepad to change color
        m_gamepad.color = playerColor;
    }

    /*
    void OnGUI()
    {
        int areaWidth = 200;
        int unitWidth = areaWidth / 4;
        int unitHeight = 20;
        int xx = 10 + 110 * m_id;
        int yy = 10;
        GUI.Box(new Rect(xx, 10, areaWidth, unitHeight), m_gamepad.Name);
        yy += unitHeight;
        GUI.Box(new Rect(xx, yy, areaWidth, unitHeight), "buttons");
        yy += unitHeight;
        for (int ii = 0; ii < m_gamepad.buttons.Length; ++ii) {
            int x = ii % 4;
            int y = ii / 4;
            GUI.Box(new Rect(xx + x * unitWidth, yy + y * unitHeight, unitWidth, unitHeight), m_gamepad.buttons[ii].pressed ? "*" : "");
        }
    }
    */
}
